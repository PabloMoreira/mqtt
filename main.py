import paho.mqtt.client as mqtt
import ssl
import asyncio
import time

class _Task_():
	def init():
		pass

	def worker():
		pass

async def _tk_loop():
	while True:
		root.update_idletasks()
		root.update()
		time.sleep(0.125)


async def _mqtt_loop():
	while True:
		client.loop(2000,max_packets=1)
		#await asyncio.sleep(1000)



# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("puerta/informatica")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.

from tkinter import Tk, Label, Button

class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple GUI")

        self.label = Label(master, text="This is our first GUI!")
        self.label.pack()

        self.greet_button = Button(master, text="Greet", command=self.greet)
        self.greet_button.pack()

        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack()

    def greet(self):
        print("Greetings!")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set("nobody", password="nopasswd")
client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
    tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect_async("mqtt.jost.com.ar", 8883, 60)
client.loop_start()


root = Tk()
my_gui = MyFirstGUI(root)



asyncio.ensure_future(_tk_loop())
asyncio.ensure_future(_mqtt_loop())


loop = asyncio.get_event_loop()
loop.run_forever()

#while True:
#    root.update_idletasks()
#    root.update()

#root.mainloop()
#    cloolient.loop(2000,max_packets=1)

